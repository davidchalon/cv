[![pipeline status](https://gitlab.com/davidchalon/cv/badges/master/pipeline.svg)](https://gitlab.com/davidchalon/cv/-/commits/master) [![coverage report](https://gitlab.com/davidchalon/cv/badges/master/coverage.svg)](https://gitlab.com/davidchalon/cv/-/commits/master)

Description
===========

This project is used to generate a résumé in html.
The input data for the résumé comes from a yaml file and the generation
process uses jinja2 template to generate the output.

For the html résumé, a boostrap frontend is used.

Dependencies
============

 * Common
    * Python
    * Jinja2
    * PyYAML
    * docutils
    * nodejs

 * HTML
    * Sass (yarn add sass)

 * PDF
    * Puppeteer (yarn add puppeteer)
    * chromium

Copying
=======

Copyright (C) 2013-2019, Aurélien Chabot <aurelien@chabot.fr>

Licensed under **GPLv3**.

See COPYING file.
