
all: web pdf

WEB_CSS := www/assets/css/cv.css
WEB_OUTPUT := www/cv.html www/cv.en.html

${WEB_OUTPUT}: generate.py *.yaml templates/*
	./generate.py

${WEB_CSS}: scss/cv.scss
	sass scss/cv.scss ${WEB_CSS}

css: ${WEB_CSS}

web: ${WEB_OUTPUT} ${WEB_CSS}

pdf: web
	node pdf.js

check: web
	html5validator --also-check-css --root www

clean:

rmproper: clean
	@rm -f www/*.pdf
	@rm -f www/*.html
