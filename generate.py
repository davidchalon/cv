#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    CV generator
    ~~~~~~~~~~~~

    :copyright: (c) 2013-2019 by Aurélien Chabot <aurelien@chabot.fr>
    :license: LGPLv3, see COPYING for more details.
"""
try:
    from docutils import core as docutils
    import jinja2
    import yaml
    import os
    import codecs
except ImportError as error:
    print('ImportError: ' + str(error))
    exit(1)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

# Settings

INPUT_EN = os.path.join(CURRENT_DIR, 'data.en.yaml')
INPUT_FR = os.path.join(CURRENT_DIR, 'data.fr.yaml')
OUTPUT_WEB = os.path.join(CURRENT_DIR, 'www')
TEMPLATE_PATH = os.path.join(CURRENT_DIR, 'templates')

TEMPLATE_OPTIONS = {}

STEPS = []


def step(func):
    """Wrapper aroung the step"""
    def wrapper(*args, **kwargs):
        func(*args, **kwargs)
    STEPS.append(wrapper)
    return wrapper


def write_file(path, data):
    """Write data to file"""
    dirs = os.path.dirname(path)
    if not os.path.isdir(dirs):
        os.makedirs(dirs)
    with codecs.open(path, mode='w', encoding='utf-8') as f:
        f.write(data)


def rst2html(data):
    """Convert rst text to html"""
    overrides = {'input_encoding': 'unicode'}
    parts = docutils.publish_parts(
        source=data, writer_name='html', settings_overrides=overrides)
    return parts['html_title'] + "\n" + parts['fragment']


def all2rst(data, tag, convert):
    """Convert data object content with the provided converter"""
    tag2convert = [ 'presentation' ]

    if isinstance(data, str):
        return convert(data) if tag in tag2convert else data
    elif isinstance(data, dict):
        return {k: all2rst(v, k, convert) for k, v in data.items()}
    elif isinstance(data, list):
        return [all2rst(v, None, convert) for v in data]
    elif isinstance(data, int):
        return data
    else:
        print("Unhandle data: " + str(data))
        return data


def load_data(f, convert):
    with open(f) as cv:
        return all2rst(yaml.load(cv, Loader=yaml.FullLoader), None, convert)


def generate_web(jinja_env, f, template, name):
    generate(jinja_env, load_data(f, rst2html), template, name, OUTPUT_WEB)


def generate(jinja_env, env, template_name, name, output):
    temp = os.path.join(TEMPLATE_PATH, name)
    out = os.path.join(output, name)
    print('  %s -> %s' % (temp, out))
    template = jinja_env.get_template(template_name)
    write_file(out, template.render(env))


@step
def step_cv_fr_web(jinja_env):
    generate_web(jinja_env, INPUT_FR, "cv.html", "cv.html")


@step
def step_cv_en_web(jinja_env):
    generate_web(jinja_env, INPUT_EN, "cv.html", "cv.en.html")


if __name__ == '__main__':
    env = jinja2.Environment(variable_start_string='{@', variable_end_string='@}',
                             trim_blocks=True, lstrip_blocks=True,
                             loader=jinja2.FileSystemLoader(TEMPLATE_PATH), **TEMPLATE_OPTIONS)
    print('* Generating...')
    for step in STEPS:
        step(env)
