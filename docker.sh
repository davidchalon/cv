#!/bin/sh

SOURCE_ROOT="$(readlink -f "$( cd "$( dirname "$0" )" && pwd )")"

# Interactive script?
if [ -t 1 ]; then
    OPT="-i"
fi

run() {
    image=$1
    shift
    docker run \
        -v $SOURCE_ROOT:$SOURCE_ROOT \
        -w $SOURCE_ROOT $OPT \
        $image sh -c "$@"
}

run "node:alpine" "apk add --update make && yarn add sass \
    && ln -s \`yarn bin sass\` /usr/local/bin/sass && make css"
run "python:alpine" "apk add --update make && pip3 install -r requirement.txt \
        && touch www/assets/css/cv.css && make web"
run "node:alpine" "apk add --update make chromium \
    && ln -s /usr/bin/chromium-browser /usr/bin/chromium \
    && PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true yarn add puppeteer@1.17.0 && make pdf"
run "python:alpine" "apk add --update make openjdk8-jre \
    && pip3 install html5validator \
    && html5validator --also-check-css --root www"
